import Config from 'react-native-config'
import axios from 'axios'

async function searchNews(page, word){
    //console.log('env : ', Config.BASE_URL)
    //console.log('key : ', Config.API_KEY)
    const req = await axios.get(`${Config.BASE_URL}/everything?q=${word}&page=${page}&apiKey=${Config.API_KEY}`)
    //console.log(req.data.articles)
    return req.data.articles
}

export default searchNews