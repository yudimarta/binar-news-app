import Config from 'react-native-config'
import axios from 'axios'

async function fetchNews(page){
    //console.log('env : ', Config.BASE_URL)
    //console.log('key : ', Config.API_KEY)
    const req = await axios.get(`${Config.BASE_URL}/top-headlines?country=id&apiKey=${Config.API_KEY}&page=${page}`)
    //console.log(req.data.articles)
    return req.data.articles
}

export default fetchNews