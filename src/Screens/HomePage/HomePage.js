import React, { Component } from 'react';
import {View, FlatList, TouchableHighlight, TouchableOpacity, Image } from 'react-native';
import fetchNews from '../../utils/fetchNews'
import { Text, Card, Divider } from 'react-native-elements';
import SearchPage from '../SearchPage/SearchPage';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { SafeAreaView } from 'react-native-safe-area-context';
import moment from 'moment';
import { styles } from '../Styling/styling'

const img = { uri : 'https://apifriends.com/wp-content/uploads/2018/04/stackoverflow.png'}

export default class HomePage extends Component{
  state = {
		articles: [],
    refreshing: true,
    loading : true,
    url : '',
    page : 1
	};

  componentDidMount(){
    this.handleNews()
  }

  handleNews(halaman){
    fetchNews(halaman).then(articles => {
      this.setState({ articles, refreshing: false})
    })
    .catch(() => this.setState({ refreshing : false}))
  }

  handleAddNews(halaman){
    fetchNews(halaman).then(articles => {
      let listData = this.state.articles
      if (articles.length === 0){
        alert('No More to Reload')
        this.setState({ articles : listData, refreshing: false})
        this.state.page = 1
      } else {
        let data = listData.concat(articles)
        this.setState({ articles : data, refreshing: false})
      }
    })
    .catch(() => this.setState({ refreshing : false}))
  }

  handleRefresh = () => {
    this.setState({ refreshing : true}, () => this.handleNews(this.state.page))
  }

  handleLoadMore = () => {
    this.state.page = this.state.page + 1
    this.setState({ refreshing : true}, () => this.handleAddNews(this.state.page))
  }

  moveScreen(url){
    const { navigation } = this.props
    //console.log(url)
    navigation.navigate('DetailScreen', {url : url})
  }

  renderItem = ({ item }) => {
    return(
      <TouchableOpacity style={styles.bodyBackgroundColor} onPress={() => {
        console.log('yang dibuka dari onPress: ', item.url)
        this.moveScreen({url : item.url})
        }}>

        <Card containerStyle={styles.cardBackgroundColor}>
          <Card.Image source = {{uri : item.urlToImage}} >
            <Text style = {styles.title}> {item.title} </Text>
          </Card.Image> 
      
          <Text style={styles.description}>
            {item.description}
          </Text>
          <Divider style={{ backgroundColor: '#DFE6E9' }} />
          <View style={styles.authorBar}>
            <Text style={styles.authorText}>
              {item.author}  |  {item.source.name.toUpperCase()}  |  {moment(item.publishedAt).format('MMM DD, YYYY')}
            </Text>
            <Icons name="heart-outline" color={'#DFE6E9'} size={ 14 } style={{margin : 5}}/>
          </View>
        </Card>
      </TouchableOpacity>
    )
  }
  
  
  render(){
    const { navigation } = this.props
    console.log('hasil : ', this.state.articles)

    const getHeader = () => {
      return (
        <View>
          <TouchableHighlight style={styles.bodyBackgroundColor}>
            <Card containerStyle={styles.cardBackgroundColor}>
              <View style={styles.searchBar}>
                <Image
                  source={img}
                  style={{height: 30, width: 30}}
                />
                <Text style = {styles.titleText}> The StackOverflow News </Text>
                <TouchableOpacity>
                  <Icons name="magnify" color={'#FFFFFF'} size={ 24 } style={{margin : 5}} onPress={() => {
                    navigation.navigate(SearchPage)
                    console.log('search dipenek')}}/>
                </TouchableOpacity>
              </View>  
            </Card>
          </TouchableHighlight>
        </View>
      )
  };


    return(   
        <SafeAreaView style={styles.bodyBackgroundColor}>
        <FlatList
          data = {this.state.articles}
          renderItem={this.renderItem}
          keyExtractor={item => item.url}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          ListHeaderComponent={getHeader}
          onEndReached={this.handleLoadMore}
        />
        </SafeAreaView>
 
    )
  }
}