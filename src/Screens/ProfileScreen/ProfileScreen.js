import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Config from 'react-native-config'
import { styles } from '../Styling/styling'
// import { WebView } from 'react-native-webview';

// // ...
// // export default class DetailScreen extends Component {
// //   render() {
// //     return(
// //         <WebView source={{ uri: 'https://reactnative.dev/' }} />
// //     );
// //   }
// // }

const profpic = { uri : Config.PHOTO_URL}

function ProfileScreen() {
    //const {url} = route.params
    return(
        // <WebView source={{ uri: url.url }} /> 
        <View style={{backgroundColor:'#332940', flex:1}}>
            <View style={styles.centerItem}>
                <Image 
                    source={profpic}
                    style={{height: 200, width: 200, borderRadius:200/2}}
                />
                <Text style={styles.tulisan}>Yudi Marta Arianto</Text>

                <Text style={styles.tulisan2}>yudi_m_arianto@telkomsel.co.id</Text>
            </View>
        </View>   
    )
}

export default ProfileScreen