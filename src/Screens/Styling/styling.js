import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    tulisan : {
        fontWeight: 'bold', 
        textAlign: 'center',
        color: '#B0A8B9',
        fontSize: 34,
        marginTop: 24
    },
    tulisan2 : {
        textAlign: 'center',
        color: '#B0A8B9',
        fontSize: 16,
        marginTop: 12
    },
    centerItem : {
        alignItems: 'center', 
        flex:1, 
        justifyContent:'center'
    },
    title : {
        marginVertical : 60,
        textAlign : 'center',
        color : 'white',
        fontWeight : 'bold',
        fontSize : 16,
        textShadowColor: '#000000',
		textShadowOffset: { width: 3, height: 3 },
        textShadowRadius: 5, 
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    description : {
        marginBottom: 10, 
        marginTop : 10, 
        color: '#b2bec3'
    },
    authorBar : {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        backgroundColor: '#4B4453'
    },
    authorText : {
        margin: 5,
        color: '#b2bec3',
        fontSize: 8
    },
    bodyBackgroundColor : {
        backgroundColor:'#332940',
        flex : 1
    },
    cardBackgroundColor : {
        backgroundColor:'#4B4453'
    },
    searchBar : {
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    titleText : {
        color : '#FFFFFF',
        fontWeight : 'bold',
        fontSize : 20,
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    textInput : {
        flexDirection: 'row', 
        justifyContent: 'center'
    },
    textInputCard : {
        backgroundColor:'#4B4453', 
        height:70
    },
    searchCardBackgroundColor : {
        backgroundColor:'#332940'
    }
})

export { styles }