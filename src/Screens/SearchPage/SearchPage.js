import React, { Component } from 'react';
import {View, FlatList, TouchableHighlight, TouchableOpacity, TextInput } from 'react-native';
import searchNews from '../../utils/searchNews'
import { Text, Card, Divider } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
import { styles } from '../Styling/styling'

export default class SearchPage extends Component{
  state = {
		articles: [],
    refreshing: false,
    loading : true,
    url : '',
    page : 1,
    word : ''
	};

  handleNews(halaman, kata){
    searchNews(halaman, kata).then(articles => {
      this.setState({ articles, refreshing: false})
      if (articles.length === 0){
        alert('No Result')
      }
      })
    .catch(() => this.setState({ refreshing : false}))
  }

  handleAddNews(halaman, kata){
    searchNews(halaman, kata).then(articles => {
      let listData = this.state.articles
      if (articles.length === 0){
        alert('No More to Reload')
        this.setState({ articles : listData, refreshing: false})
        this.state.page = 1
      } else {
        let data = listData.concat(articles)
        this.setState({ articles : data, refreshing: false})
      }
    })
    .catch(() => this.setState({ refreshing : false}))
  }

  handleRefresh = () => {
    this.setState({ refreshing : true}, () => this.handleNews(this.state.page, this.state.word))
  }

  handleLoadMore = () => {
    this.state.page = this.state.page + 1
    this.setState({ refreshing : true}, () => this.handleAddNews(this.state.page, this.state.word))
  }

  moveScreen(url){
    const { navigation } = this.props
    //console.log(url)
    navigation.navigate('DetailScreen', {url : url})
  }

  renderItem = ({ item }) => {
    return(
      <View style={styles.bodyBackgroundColor}>
        <TouchableOpacity onPress={() => {
            console.log('yang dibuka dari onPress: ', item.url)
            this.moveScreen({url : item.url})
          }}>
          <Card containerStyle={styles.cardBackgroundColor}>
            <Card.Image source = {{uri : item.urlToImage}} >
              <Text style = {styles.title}> {item.title} </Text>
            </Card.Image> 
      
            <Text style={styles.description}>
              {item.description}
            </Text>
            <Divider style={{ backgroundColor: '#DFE6E9' }} />
            <View style={styles.authorBar}>
              <Text style={styles.authorText}>
                {item.author}  |  {item.source.name.toUpperCase()}  |  {moment(item.publishedAt).format('MMM DD, YYYY')}
              </Text>
              <Icons name="heart-outline" color={'#DFE6E9'} size={ 16 } style={{margin : 5}}/>
            </View>
          </Card>
        </TouchableOpacity>
      </View>  
    )
  }

  render(){
    const { navigation } = this.props
    console.log('hasil : ', this.state.articles)
    console.log(this.state.word)

    return(
      <View style={styles.bodyBackgroundColor}>
        <TouchableHighlight style={styles.searchCardBackgroundColor}>
          <Card containerStyle={styles.textInputCard}>
            <View style={styles.textInput}>
              <TextInput style={{fontSize:15}}
                placeholder="Search Here"
                placeholderTextColor="#b2bec3"
                textAlign='center'
                onChangeText={(word) => this.setState({word})}
                onSubmitEditing={() => this.handleRefresh()}
                keyboardType='web-search'
                value={this.state.word}
                autoFocus={true}
                color='white'
              />
            </View>  
          </Card>
        </TouchableHighlight>

        <FlatList
          backgroundColor='#332940'
          data = {this.state.articles}
          contentContainerStyle={{ flexGrow: 1 }}
          renderItem={this.renderItem}
          keyExtractor={item => item.url}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          onEndReachedThreshold={0.4}
          onEndReached={this.handleLoadMore}
        />
      </View>
    )
  }
}