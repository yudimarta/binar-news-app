import React from 'react';
import {View, Text} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomePage from './Screens/HomePage/HomePage.js'
import SearchPage from './Screens/SearchPage/SearchPage.js'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import DetailScreen from './Screens/DetailScreen/DetailScreen.js'
import ProfileScreen from './Screens/ProfileScreen/ProfileScreen.js'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function MyTab() {
  return (
      <Tab.Navigator initialRouteName="HomePage" tabBarOptions={{ activeTintColor : 'white', activeBackgroundColor : '#527B73', inactiveBackgroundColor : '#332940'}}>
        <Tab.Screen 
            name="HomePage" 
            component={HomePage} 
            options={{ tabBarLabel : 'Home', 
            tabBarIcon: ({ color, size}) => (
                <Icons name="home" color={ color } size={ size } />
            )}}/>
        <Tab.Screen 
            name="SearchPage" 
            component={SearchPage} 
            options={{ tabBarLabel : 'Search', 
            tabBarIcon: ({ color, size}) => (
                <Icons name="file-search-outline" color={ color } size={ size } />
            )}}/>
        <Tab.Screen 
            name="ProfileScreen" 
            component={ProfileScreen} 
            options={{ tabBarLabel : 'Profile', 
            tabBarIcon: ({ color, size}) => (
                <Icons name="account" color={ color } size={ size } />
            )}}/>
      </Tab.Navigator>
  );
}

const MyStack = () => {
  return (
      <SafeAreaProvider>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="HomePage" headerMode="none">
            <Stack.Screen name="HomePage" component={MyTab} />
            <Stack.Screen name="DetailScreen" component={DetailScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
  );
}

export default MyStack;
