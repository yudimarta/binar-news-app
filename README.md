# binar-news-app

### REGISTER:
news api: https://newsapi.org/ (done)

### Docs:
https://newsapi.org/docs/endpoints (done)

### Page:
1. HOME: {{baseUrl}}/top-headlines?country=id&apiKey={{apiKey}}&page=1 (done)
2. SEARCH: {{baseUrl}}/everything?q=telkomsel&page=1&apiKey={{apiKey}} (done)

### Requirement:
1. WebView (done)
2. Unlimited Scroll using FlatList (done)
3. Dalam 1 Card: Title, Description, Image, Date, Author (done)
4. When Seach hit on press button search, keyboardType ```search``` (done)